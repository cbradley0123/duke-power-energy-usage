# Duke Power Energy

This is a simple script for retrieving your power usage from Duke Power for the current month. It is specifically written for the Duke Energy side of their operation.  

The code is written for Python 3.4.2.

To use this, you will need to have a duke.conf file in the same directory.  The .conf file will need to have 3 values.
duke_uid = <login username>, 
duke_pwd = <login password>, 
duke_acct = <your account number>

This script will return two lists.  One for temperature, one for energy used in KwH.  The values are in order for the days of the current month.  This will cover up to previous day.

----- Licence ------

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see https://www.gnu.org/licenses.