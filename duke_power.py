# ----- Licence ------

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see https://www.gnu.org/licenses.

# Author: Clayton Bradley
# Date: 7/7/2018
# Python version: 3.4.2
# Purpose: Log into duke-energy.com and retrieve current month usage information
# Returns: Two lists, on of temperature for the day and one for energy usage for the day

import requests
import re
import imp
import time
import datetime
import json

print('----------------------------')
print('Retrieving Duke Power Usage.')
print(datetime.datetime.now())

#start the timer for execution
start_time = time.clock()

# for bringing in uid / pwd / acct.  .conf will not be included in git
# values in the .conf are duke_uid, duke_pwd, duke_acct
duke = imp.load_source('duke', 'duke.conf')

# parameters sent when logging in.  To this will be added hidden variables.  
# deviceprofile and pageId, specifically.  They are found on the login page.
login_payload = {
	'userId':	duke.duke_uid,
	'userPassword':	duke.duke_pwd
}

# parameters sent when looking for electric usage.  The only one that needs to change
# is the Date.  Note the space before and after the / in Date, but NOT in ActiveDate.
data_payload = {
	'ActiveDate':	'03/29/2017',
	'BillingFrequency':	'Month',
	'Date':	'{} / 15 / 2018'.format(datetime.datetime.now().strftime("%m")),
	'Graph':	'DailyEnergy',
	'GraphText':	'Daily Energy and Avg.',
	'MeterNumber':	'ELECTRIC - {}'.format(duke.duke_acct)
}

# by using session, we are going to retain cookies between requests
session_requests = requests.session()

# where we get the hidden fields
url = 'https://www.duke-energy.com/my-account/sign-in'

# where we send the login information
login_url = 'https://www.duke-energy.com/form/Login/GetAccountValidationMessage'

# have to hit this one before we can request data
intermediate_url = 'https://www.duke-energy.com/my-account/usage-analysis'

# get the data
data_url = 'https://www.duke-energy.com/api/UsageAnalysis/GetUsageChartData'

# mimic Safari with these headers
user_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.95 Safari/537.36'
accept = 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8'
accept_encoding = 'gzip, deflate, br'

# get the login page which has hidden fields
result = session_requests.get(
	url,
	headers = {'User-Agent': user_agent, 'Accept-Encoding': accept_encoding, 'Accept': accept}
)

# regular expression to find hidden field names and values
find_hidden_login_vals = re.compile(r'<input type="hidden" name="(.*?)" value="(.*?)" />')

# search the login page for all hidden fields
hidden_logins = re.findall(find_hidden_login_vals, result.text)

# go through the hidden field name/value and add them to our login_payload
for hidden in hidden_logins:
	login_payload[hidden[0]] = hidden[1]

print('Logging in.')

# send the login request using the login payload
result = session_requests.post(
	login_url, 
	data = login_payload  
)

# check for status code 200 which indicates a successful login, if we dont have it, log it and exit.
if result.status_code != 200:
	print('Failed to login.  Status code {} returned.\nExiting.'.format(result.status_code))
	exit()

print('Logged in.')
print('Going to intermediate url.')

# we must hit the intermediate URL.  
result = session_requests.get(
	intermediate_url
)

# again, check for status code 200.  If not there, exit.
if result.status_code != 200:
	print ('Failed to connect to intermediate page.  Status code {} returned.\nExiting.'.format(result.status_code))
	exit()
print('Connected to intermediate url.')
print('Sending request for data.')

# this is what we came for.  Retrieve the data as specified in the data_payload.
result = session_requests.post(
	data_url, 
	data = data_payload 
)

# Check for status code 200.  If not there, exit.
if result.status_code != 200:
	print ('Failed to connect to data page.  Status code {} returned.\nExiting.'.format(result.status_code))
	exit()
print('Data retrieved.')
print('Closing connection.')
print('Finished.')
info = json.loads(result.text)
print('Temperatures: {}'.format(info['temperatures']))
print('Usage: {}'.format(info['meterData']['Electric']))
print ('Execution time: {} seconds.'.format(time.clock() - start_time))
